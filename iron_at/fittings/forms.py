"""Custom forms for fitting manager.

Copyright (c) 2017, Arun Seehra <seehra.a@gmail.com>
All rights reserved.
"""

from django import forms
from django.forms import inlineformset_factory

from .models import Fit, FitSkill

# pylint: disable=invalid-name
FitSkillFormSet = inlineformset_factory(Fit, FitSkill, fields=['skill_name'])
# pylint: enable=invalid-name


class XUpForm(forms.Form):
    """A form for x-ing up for a particular fit"""

    sign_up = forms.BooleanField(required=False)
    t2_capable = forms.BooleanField(required=False)

    def clean(self):
        cleaned_data = super(XUpForm, self).clean()
        if not cleaned_data.get('sign_up'):
            return cleaned_data

        skill_field_keys = [key for key in cleaned_data.copy().keys()
                            if key.startswith('skill_')]
        for field_name in skill_field_keys:
            if cleaned_data[field_name] is None:
                self.add_error(field_name, 'Required field.')
