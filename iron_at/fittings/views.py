# pylint: disable=too-many-ancestors

from django import forms
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.forms import formset_factory
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.urls import reverse, reverse_lazy
from django.views.generic import (CreateView, DeleteView, DetailView, FormView,
                                  ListView, TemplateView, UpdateView)

from .forms import FitSkillFormSet, XUpForm
from .models import Composition, Fit, XUp, XUpSkill


class CompositionCreateView(PermissionRequiredMixin, CreateView):
    """Simple generic view to create a new fleet comp."""

    model = Composition
    fields = ['name', 'description']
    permission_required = 'fittings.add_composition'

    def form_valid(self, form):
        form.instance.owner = self.request.user
        return super(CompositionCreateView, self).form_valid(form)


class CompositionEditView(PermissionRequiredMixin, UpdateView):
    """Simple generic view to edit an existing fleet comp."""

    model = Composition
    fields = ['name', 'description']
    permission_required = 'fittings.change_composition'


class CompositionDetailsView(PermissionRequiredMixin, DetailView):
    """Simple generic view to display a fleet comp."""

    model = Composition
    permission_required = ['fittings.view_composition', 'fittings.view_fit']


class CompositionDeleteView(PermissionRequiredMixin, DeleteView):
    """Simple generic view to delete a fleet comp."""

    model = Composition
    success_url = reverse_lazy('fittings:index')
    permission_required = 'fittings.delete_composition'


class IndexView(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    """Simple generic view for displaying all the fleet comps."""

    model = Composition
    template_name = 'fittings/index.html'
    permission_required = 'fittings.view_composition'


class FitCreateView(PermissionRequiredMixin, CreateView):
    """View for adding a fit to a fleet composition"""
    model = Fit
    fields = ['eft_fit', 'quantity']
    permission_required = ['fittings.add_fit', 'fittings.add_fitskill']

    def get(self, request, *args, **kwargs):
        self.skills_formset = FitSkillFormSet(instance=Fit())
        return super(FitCreateView, self).get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        self.skills_formset = FitSkillFormSet(request.POST, instance=Fit())
        return super(FitCreateView, self).post(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(FitCreateView, self).get_context_data(**kwargs)
        comp = Composition.objects.get(pk=self.kwargs['comp_id'])
        context['composition'] = comp
        context['skills_formset'] = self.skills_formset
        return context

    def form_valid(self, form):
        form.instance.comp = Composition.objects.get(pk=self.kwargs['comp_id'])
        self.object = form.save()
        self.skills_formset = FitSkillFormSet(self.request.POST, instance=self.object)
        if not self.skills_formset.is_valid():
            return self.form_invalid(form)
        self.skills_formset.save()
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse('fittings:composition-details', args=[self.kwargs['comp_id']])


class FitEditView(PermissionRequiredMixin, UpdateView):
    """View for updating a fit's details."""

    model = Fit
    fields = ['eft_fit', 'quantity']
    permission_required = ['fittings.change_fit',
                           'fittings.add_fitskill',
                           'fittings.delete_fitskill',
                           'fittings.change_fitskill']

    def get(self, request, *args, **kwargs):
        self.skills_formset = FitSkillFormSet(instance=self.get_object())
        return super(FitEditView, self).get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        self.skills_formset = FitSkillFormSet(request.POST,
                                              instance=self.get_object())
        return super(FitEditView, self).post(request, *args, **kwargs)

    def form_valid(self, form):
        if not self.skills_formset.is_valid():
            return self.form_invalid(form)
        self.object = form.save()
        self.skills_formset.save()
        return HttpResponseRedirect(self.get_success_url())

    def get_context_data(self, **kwargs):
        context = super(FitEditView, self).get_context_data(**kwargs)
        comp = Composition.objects.get(pk=self.kwargs['comp_id'])
        context['composition'] = comp
        context['skills_formset'] = self.skills_formset
        return context

    def get_success_url(self):
        return reverse('fittings:composition-details', args=[self.kwargs['comp_id']])


class FitDeleteView(PermissionRequiredMixin, DeleteView):
    """A simple delete-confirmation view for Fits."""

    model = Fit
    success_url = reverse_lazy('fittings:index')
    permission_required = 'fittings.delete_fit'


def skill_id_str(skill):
    return 'skill_{}'.format(skill.id)


def build_initial(x_up):
    if not x_up:
        return None
    initial = {'sign_up': True,
               't2_capable': x_up.t2_capable}

    skill_id_values = {skill_id_str(skill.skill): skill.skill_value
                       for skill in x_up.xupskill_set.all()}

    initial.update(skill_id_values)
    return initial


class XUpView(FormView):
    template_name = 'fittings/xup.html'
    form_class = XUpForm

    def get_fit(self):
        return get_object_or_404(Fit, pk=self.kwargs['fit_id'])

    def get(self, request, *args, **kwargs):
        self.fit = self.get_fit()
        return super(XUpView, self).get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        self.fit = self.get_fit()
        return super(XUpView, self).post(request, *args, **kwargs)

    def get_form(self, form_class=None):
        form = super(XUpView, self).get_form(form_class)
        # At this point, the form is fully initialized, so we can add our custom
        # fields
        for skill in self.fit.skills.all():
            form.fields[skill_id_str(skill)] = forms.IntegerField(
                label=skill.skill_name,
                min_value=0,
                max_value=5,
                required=False)
        return form

    def get_initial(self):
        x_up = XUp.objects.filter(fit=self.fit, pilot=self.request.user).first()
        return build_initial(x_up)

    def get_context_data(self, **kwargs):
        if 'fit' not in kwargs:
            kwargs['fit'] = self.fit
        return super(XUpView, self).get_context_data(**kwargs)

    def get_success_url(self):
        return reverse('fittings:composition-details', args=[self.kwargs['comp_id']])

    def form_valid(self, form):
        if not form.has_changed():
            return HttpResponseRedirect(self.get_success_url())

        if not form.cleaned_data['sign_up']:
            XUp.objects.filter(fit=self.fit, pilot=self.request.user).delete()
            return HttpResponseRedirect(self.get_success_url())

        x_up, created = XUp.objects.update_or_create(
            fit=self.fit,
            pilot=self.request.user,
            defaults={'t2_capable': form.cleaned_data['t2_capable']})

        for skill in self.fit.skills.all():
            x_up_skill = XUpSkill.objects.update_or_create(
                x_up=x_up,
                skill=skill,
                defaults={'skill_value': form.cleaned_data[skill_id_str(skill)]})

        return HttpResponseRedirect(self.get_success_url())
