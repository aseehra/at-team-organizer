"""Py.Test tests for views.

Copyright (c) 2017, Arun Seehra <seehra.a@gmail.com>
All rights reserved.
"""
# pylint: disable=missing-docstring,invalid-name

from django.urls import reverse

import pytest

# pylint: disable=import-error
from iron_at.fittings import models
# pylint: enable=import-error

@pytest.fixture
def comp():
    return models.Composition.objects.create(
        name="Test Composition", description="testing.")

@pytest.mark.django_db
class TestFitCreateView:
    """Tests for adding a fit & skillsets."""

    def test_get__should_fill_context_with_composition_and_formset(
            self, admin_client, comp):
        response = admin_client.get(reverse('fittings:fit-create', args=[comp.id]))
        assert response.context_data['composition'] == comp
        assert response.context_data['skills_formset'] is not None

    def test_post__should_save_fit_given_empty_skills(self, admin_client, comp):
        data = {
            'eft_fit': 'EFT DATA',
            'quantity': 1,
            'skills-TOTAL_FORMS': '1',
            'skills-INITIAL_FORMS': '0',
            'skills-MAX_FORMS': '',
            'skills-0-skill_name': ''
        }
        admin_client.post(reverse('fittings:fit-create', args=[comp.id]),
                          data=data)
        fits = comp.fits.all()

        assert len(fits) == 1
        assert fits[0].eft_fit == 'EFT DATA'
        assert fits[0].quantity == 1
        assert len(fits[0].skills.all()) == 0

    def test_post__should_save_skills_given_one_formset(
            self, admin_client, comp):
        data = {
            'eft_fit': 'EFT DATA',
            'quantity': 1,
            'skills-TOTAL_FORMS': '1',
            'skills-INITIAL_FORMS': '0',
            'skills-MAX_FORMS': '',
            'skills-0-skill_name': 'Test Skill'
        }
        admin_client.post(reverse('fittings:fit-create', args=[comp.id]),
                          data=data)

        fits = comp.fits.all()
        assert len(fits) == 1
        skills = fits[0].skills.all()
        assert len(skills) == 1
        assert skills[0].skill_name == 'Test Skill'
