"""Py.Test tests for model objects.

Copyright (c) 2017, Arun Seehra <seehra.a@gmail.com>
All rights reserved.
"""
# pylint: disable=missing-docstring,invalid-name

# pylint: disable=import-error
from iron_at.fittings.models import Fit\
# pylint: enable=import-error

def test_ship_name__should_extract_ship_name():
    eft_text = '[Caldari Navy Griffin, My Griffin Fit]\nRubbish\n'
    assert Fit(eft_fit=eft_text).ship_name() == 'Caldari Navy Griffin'


def test_ship_name__should_titlecase_name():
    eft_text = '[caldari navy griffin, my griffin fit]\nRubbish\n'
    assert Fit(eft_fit=eft_text).ship_name() == 'Caldari Navy Griffin'


def test_ship_name__given_unnamed_fit_should_extract_ship_name():
    eft_text = '[Caldari Navy Griffin]\nRubbish\n'
    assert Fit(eft_fit=eft_text).ship_name() == 'Caldari Navy Griffin'


def test_ship_name__given_malformed_ship_should_return_null():
    eft_text = '[, Caldari Navy Griffin]\nRubbish\n'
    assert Fit(eft_fit=eft_text).ship_name() is None
    eft_text = '[Caldari\n Navy Griffin, My Favorite!]\nRubbish\n'
    assert Fit(eft_fit=eft_text).ship_name() is None
