"""
Settings for production/deployment.
"""

from .base import *  # pylint: disable=W0614, W0401

# SECRETS CONFIGURATION
# ------------------------------------------------------------------------------
SECRET_KEY = env('DJANGO_SECRET_KEY')


# SSL CONFIGURATION
# ------------------------------------------------------------------------------
USE_X_FORWARDED_HOST = True
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')
SESSION_COOKIE_SECURE = True
CSRF_COOKIE_SECURE = True
SOCIAL_AUTH_REDIRECT_IS_HTTPS = True


# SITE CONFIGURATION
# ------------------------------------------------------------------------------
DOMAIN_NAME = env('DJANGO_DOMAIN_NAME', default='fleep.thirtyfivemm.com')
ALLOWED_HOSTS = env.list('DJANGO_ALLOWED_HOSTS', default=[DOMAIN_NAME,])


# EMAIL CONFIGURATION
# ------------------------------------------------------------------------------
ADMINS = [
    ('Arun Seehra', 'arun@thirtyfivemm.com'),
]

DEFAULT_FROM_EMAIL = 'Iron AT Team <noreply@{}>'.format(DOMAIN_NAME)
SERVER_EMAIL = DEFAULT_FROM_EMAIL
#EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

# DATABASE CONFIGURATION
# ------------------------------------------------------------------------------
DATABASES['default'] = env.db('DATABASE_URL')


# STATIC FILES CONFIGURATION
# ------------------------------------------------------------------------------
STATIC_ROOT = env('DJANGO_STATIC_ROOT', default='/var/www/html/{}'.format(DOMAIN_NAME))
