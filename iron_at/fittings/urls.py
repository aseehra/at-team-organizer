"""URL configuration for the fitting manager.

Copyright (c) 2017, Arun Seehra <seehra.a@gmail.com>
All rights reserved.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$',
        views.IndexView.as_view(),
        name='index'),
    url(r'^create/$',
        views.CompositionCreateView.as_view(),
        name='composition-create'),
    url(r'^(?P<pk>\d+)/$',
        views.CompositionDetailsView.as_view(),
        name='composition-details'),
    url(r'^(?P<pk>\d+)/edit/$',
        views.CompositionEditView.as_view(),
        name='composition-edit'),
    url(r'^(?P<pk>\d+)/delete/$',
        views.CompositionDeleteView.as_view(),
        name='composition-delete'),
    url(r'^(?P<comp_id>\d+)/fit/create/$',
        views.FitCreateView.as_view(),
        name='fit-create'),
    url(r'^(?P<comp_id>\d+)/fit/(?P<pk>\d+)/edit/$',
        views.FitEditView.as_view(),
        name='fit-edit'),
    url(r'^(?P<comp_id>\d+)/fit/(?P<pk>\d+)/delete/$',
        views.FitDeleteView.as_view(),
        name='fit-delete'),
    url(r'^(?P<comp_id>\d+)/fit/(?P<fit_id>\d+)/xup/$',
        views.XUpView.as_view(),
        name='fit-xup'),
]
#pylint: enable=invalid-name
