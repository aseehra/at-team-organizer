"""
Settings for local development.
"""

from .base import *  # pylint: disable=W0614, W0401

# DEBUG
# ------------------------------------------------------------------------------
DEBUG = env.bool('DJANGO_DEBUG', default=True)
TEMPLATES[0]['OPTIONS']['debug'] = DEBUG


# SECRETS CONFIGURATION
# ------------------------------------------------------------------------------
SECRET_KEY = env('DJANGO_SECRET_KEY', default='is8ux!)x_kj-6)&@f*(3&o0*^y+3m01qs!=#i!c_(^sw6y0d0s')


INTERNAL_IPS = ['127.0.0.1']
