"""
Settings for running tests efficiently.
"""

from .base import *  # pylint: disable=W0614, W0401

# SECRET CONFIGURATION
# ------------------------------------------------------------------------------
SECRET_KEY = env('DJANGO_SECRET_KEY', default='CHANGEME!!!')


# TEMPLATE LOADERS
# ------------------------------------------------------------------------------
# Keep templates in memory so tests run faster
TEMPLATES[0]['OPTIONS']['loaders'] = [
    ('django.template.loaders.cached.Loader', [
        'django.template.loaders.filesystem.Loader',
        'django.template.loaders.app_directories.Loader',
    ]),
]


# EMAIL BACKEND
# ------------------------------------------------------------------------------
EMAIL_BACKEND = 'django.core.mail.backends.locmem.EmailBackend'


# PASSWORD HASHING
# ------------------------------------------------------------------------------
# Purposely use insecure hashing for testing speed
PASSWORD_HASHERS = [
    'django.contrib.auth.hashers.MD5PasswordHasher',
]


# COMPRESSOR CONFIGURATION
# ------------------------------------------------------------------------------
# This may need to change if we start doing selenium testing
# https://django-compressor.readthedocs.io/en/latest/settings/#base-settings
COMPRESS_ENABLED = False
COMPRESS_PRECOMPILERS = ()
