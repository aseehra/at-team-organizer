"""Admin sites for the fittings manager.

Copyright (c) 2017, Arun Seehra <seehra.a@gmail.com>
All rights reserved.
"""
from django.contrib import admin

from .models import Composition, Fit, FitSkill, XUp, XUpSkill

# Register your models here.
admin.site.register(Composition)

class FitSkillAdmin(admin.StackedInline):
    """Inline for FitSkills."""
    model = FitSkill


@admin.register(Fit)
class FitAdmin(admin.ModelAdmin):
    """Admin object that contains an inline for FitSkills."""
    inlines = [FitSkillAdmin,]


class XUpSkillAdmin(admin.StackedInline):
    """Inline for XUpSkills."""
    model = XUpSkill


@admin.register(XUp)
class XUpAdmin(admin.ModelAdmin):
    """Admin object that contains an inline for XUpSkills."""
    inlines = [XUpSkillAdmin,]
