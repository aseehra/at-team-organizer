"""The database models for the fittings manager.

Copyright (c) 2017, Arun Seehra <seehra.a@gmail.com>
All rights reserved.
"""
import re

from django.conf import settings
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from django.shortcuts import reverse


class Composition(models.Model):
    """The representation of a AT Fleet Composition.

    Attributes:
        name: The name of the fleet comp.
        description: User added details about the composition.
        owner: The user that this composition, or null if the user has been
            subsequently deleted.
        fits: The RelatedManager for Fit entries associated with this fleet
            comp.
    """

    name = models.CharField(max_length=80)
    description = models.TextField()
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, models.SET_NULL, null=True)

    class Meta:  # pylint: disable=too-few-public-methods,missing-docstring
        default_permissions = ('view', 'add', 'change', 'delete')

    def get_absolute_url(self):
        """Get the URL form each object in order to allow generic views."""
        return reverse('fittings:composition-details', kwargs={'pk': self.pk})

    def __str__(self):
        return str(self.name)


class Fit(models.Model):
    """The representation of an EVE Online ship fit.

    Attributes:
        comp: The fleet comp this fit belongs to. Can be null to allow for
            testing.
        eft_fit: An EFT-formatted string that describes the fit.
        quantity: The number of ships of this fit that should appear in the
            fleet comp.
        num_skill: The number of hull skills. E.g. a Kitsune has two skills:
            Caldari Frigate and Electronic Attack Ships. This is brittle as
            it does not handle T3 cruisers properly.
        x_ups: The RelatedManager for XUp entries associated with this fit.
        skills: The RelatedManager for FitSkill entries associated with this
            fit.
    """

    comp = models.ForeignKey(Composition, models.CASCADE, related_name='fits', null=True)
    eft_fit = models.TextField()
    quantity = models.SmallIntegerField(validators=[MinValueValidator(1),
                                                    MaxValueValidator(2)])

    class Meta:  # pylint: disable=too-few-public-methods,missing-docstring
        default_permissions = ('view', 'add', 'change', 'delete')

    def ship_name(self):
        """Return the ship hull used for this ship."""
        matches = re.search(r'\[([\w\d\s]+)(,.*)?\]', str(self.eft_fit).splitlines()[0])
        if matches:
            return matches.group(1).title()
        return None

    def __str__(self):
        return '{}.{}'.format(self.comp, self.ship_name())


class FitSkill(models.Model):
    """The skill needed to fly a particular fit.

    Attributes:
        skill_name: The name of the skill, e.g. "Caldari Frigate."
        fit: The fit (and thus ship) that this skill corresponds to.
    """

    skill_name = models.CharField(max_length=80)
    fit = models.ForeignKey(Fit, models.CASCADE, related_name='skills')

    class Meta:  # pylint: disable=too-few-public-methods,missing-docstring
        default_permissions = ('view', 'add', 'change', 'delete')

    def __str__(self):
        return "{} - {}".format(self.fit, self.skill_name)


class XUp(models.Model):
    """The representation of a pilot signing up for a particular fit.

    Attributes:
        pilot: The user that has signed up.
        fit: The fit to be signed up for.
        t2_capable: Whether the pilot is capable of flying the fit with all T2
            modules.
        skills: a ManyToMany relationship between XUps and FitSkills with extra
            data provided in XUpSkill.
    """

    pilot = models.ForeignKey(settings.AUTH_USER_MODEL, models.CASCADE)
    fit = models.ForeignKey(Fit, models.CASCADE, related_name='x_ups')
    t2_capable = models.BooleanField()
    skills = models.ManyToManyField(FitSkill, through='XUpSkill')

    class Meta:  # pylint: disable=too-few-public-methods,missing-docstring
        default_permissions = ('view', 'add', 'change', 'delete')

    def min_skill(self):
        """Get the loweset skill value associated with this fit."""
        # pylint: disable=no-member
        skills = [skill.skill_value for skill in self.xupskill_set.all()]
        # pylint: enable=no-member
        return min(skills)

    def __str__(self):
        return '{} - {}'.format(self.fit, self.pilot.get_full_name())


class XUpSkill(models.Model):
    """The representation of a pilot's skills with regard to a fit.

    Attributes:
        x_up: The signup that this entry corresponds with.
        skill: The skill that this entry corresponds with.
        skill_value: The pilot's skill level.
    """

    x_up = models.ForeignKey(XUp, models.CASCADE)
    skill = models.ForeignKey(FitSkill, models.CASCADE)
    skill_value = models.SmallIntegerField(validators=[MinValueValidator(0),
                                                       MaxValueValidator(5)])

    class Meta:  # pylint: disable=too-few-public-methods,missing-docstring
        default_permissions = ('view', 'add', 'change', 'delete')

    def __str__(self):
        return str(self.skill)
