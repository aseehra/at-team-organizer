"""The database models to enable EVE Authentication.

Copyright (c) 2017, Arun Seehra <seehra.a@gmail.com>
All rights reserved.
"""

from django.contrib.auth.models import AbstractUser

# Create your models here.
class EveUser(AbstractUser):
    """A placeholder for a custom user model.
    
    For now, it is not necessary to have any additional field, we just need
    a user name. Eventually we might want to extend this, so having this will
    enable that.
    """
    pass
