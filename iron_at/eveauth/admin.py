"""The views needed to administer an Eve user.

Copyright (c) 2017, Arun Seehra <seehra.a@gmail.com>
All rights reserved.
"""

from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from . import models


admin.site.register(models.EveUser, UserAdmin)
